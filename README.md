### cosmotor:  COSMOlogical evolution of models with TORsion ###


Copyright (C) 2020 Krzysztof Bolejko

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


### Compiling and testing ###

In order to use the code, you will need a fortran compiler (for example gfortran) and a plotting software (for example gnuplot).

Copy to code (file "cosmotor.f") to your working directory and type

   gfortran -o cosmotor cosmotor.f

Run the code by typing

   ./cosmotor

To plot the results, copy the gnuplot scripts (files ending with ".gpl") to the same directory and type

   gnuplot plot3.pgl 

this will produce a figure similar to Figure 3 of arXiv:2003.06528

   gnuplot plot4.pgl 

this will produce a figure similar to Figure 4 of arXiv:2003.06528

   gnuplot plot5.pgl 

this will produce a figure similar to Figure 5 of arXiv:2003.06528

   gnuplot plot6.pgl 

this will produce a figure similar to Figure 6 of arXiv:2003.06528



### Using and modifying the code ###

The relevant subroutines are:

- get_parameters:
this subroutine specify the cosmological parameters and units. Please modify this subroutine if you want to change the parameters.

- torsion_trans_com_distance
this subroutine calculates the evolution of homogeneous and isotropic universe with torsion (i.e. density, expansion rate, and scale factor), it addition it also solves for the transverse comoving distance. The initial conditions are specified at the present instant (based on the cosmological parameters from the subroutine "get_parameters") and the evolution is traced back in time. The output is: redshift: z; transverse comoving distance: dtc; departure from the distance duality relation: eta, and the Clarkson-Bassett-Lu function: CBL.

- FLRW_trans_com_distance:
this subroutine solves for the transverse comoving distance within the FLRW model. It uses the cosmological parameters (cpar) and redshift (z) as input, and it produces the transverse comoving distance (dtc).

