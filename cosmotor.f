        program COSMOTOR

! this code reproduces Figures 3-6 from paper arXiv:2003.06528

!     AIM: the code evaluates the COSMOlogical evolution of models with
!     TORsion and also provides the distance-redshift relation
!
!     AUTHOR: Krzysztof Bolejko (2020) + OTHER AUTHORS below
!
!     LICENCE: GNU General Public License version 3 or any later version.
!     disclaimer: There is NO warranty; not even for MERCHANTABILITY or FITNESS
!     FOR A PARTICULAR PURPOSE.
!     See https://www.gnu.org/licenses/gpl-3.0.en.html

!     OTHER AUTHORS:
!     2020-03-11 Boud Roukema - untabify; clearer indenting;
!     avoid dependence on exact equality of two floating point values;
!     use if-elseif-then structure to avoid assuming that software
!     errors are impossible.

! script for reproducing Figure 3
        call get_figure3

! script for reproducing Figure 4
        call get_figure4

! script for reproducing Figure 5
        call get_figure5

! script for reproducing Figure 6
        call get_figure6

!     to create the .eps files:
!  gnuplot fig3.gpl; gnuplot fig4.gpl; gnuplot fig5.gpl; gnuplot fig6.gpl

        end


! ==============================================================
        subroutine get_figure3
! this subroutine produces data for Figure 3 from arXiv:2003.06528
! after the program executes run: "gnuplot plot3.pgl" to get an eps file

        implicit none
        integer I
        double precision cpar(10)
        double precision zi,eta1,eta2,eta3


        open(30,file='fig3')

! cosmological parameters
        call get_parameters(cpar)

        do I=1,300
                zi = I*1d-2
                eta1 = cpar(6)*zi
                eta2 = cpar(6)*zi/(1.0+zi)
                eta3 = cpar(6)*log(1.0+zi)

                write(30,*) zi,eta1,eta2,eta3
        enddo

        end


! ==============================================================
        subroutine get_figure4
! this subroutine produces data for Figure 4 from  arXiv:2003.06528
! after the program executes run: "gnuplot plot4.pgl" to get an eps file

        implicit none
        integer I
        double precision cpar(10)
        integer, parameter :: N = 500
        double precision z(N),dtc(N),eta(N),CBL(3,N)
        double precision zi,dtc0,mag1,mag2,dts



        open(40,file='fig4')


! cosmological parameters
        call get_parameters(cpar)

! transverse comoving distance-redshift relation
        dts = -8d-3
        call torsion_trans_com_distance(cpar,dts,N,z,dtc,eta,CBL)


! the transverse comoving distance is compared with the FLRW model
! with the same Omega_m, Omega_lambda, and no torsion:
        cpar(2) = cpar(2) ! Omega_matter the same as with torsion
        cpar(3) = cpar(3) ! Omega_lambda the same as with torsion
        cpar(4) = 1.0 - cpar(2)-cpar(3) ! no torsion means omega_k = 1 - omega_m - omega_l


        do I=1,N
                zi = z(I)
                call FLRW_trans_com_distance(cpar,zi,dtc0)

                mag1 = 5.0*log10(1.0+eta(I))
                mag2 = 5.0*log10(dtc(I)/dtc0)


                write(40,*) zi,mag1,mag2,mag1+mag2

        enddo


        end


! ==============================================================
        subroutine get_figure5
! this subroutine produces data for Figure 5 from  arXiv:2003.06528
! after the program executes run: "gnuplot plot5.pgl" to get an eps file

        implicit none
        integer I
        double precision cpar(10)
        integer, parameter :: N = 400
        double precision z(N),dtc(N),eta(N),CBL(3,N)
        double precision zi,dtc0,dts,dh



        open(50,file='fig5')


! cosmological parameters
        call get_parameters(cpar)

! transverse comoving distance-redshift relation
        dts = -2d-3
        call torsion_trans_com_distance(cpar,dts,N,z,dtc,eta,CBL)


! the transverse comoving distance is compared with the FLRW model
! with the same Omega_m, Omega_lambda, and no torsion:
        cpar(2) = cpar(2) ! Omega_matter the same as with torsion
        cpar(3) = cpar(3) ! Omega_lambda the same as with torsion
        cpar(4) = 1.0 - cpar(2)-cpar(3) ! no torsion means omega_k = 1 - omega_m - omega_l


        do I=1,N
                zi = z(I)
                call FLRW_trans_com_distance(cpar,zi,dtc0)

                dh = cpar(9)*((dtc0/dtc(I))/(1.0+eta(I)))

                write(50,*) zi,dh
        enddo



        end

! ============================
        subroutine get_figure6
! this subroutine produces data for Figure 4 from  arXiv:2003.06528
! after the program executes run: "gnuplot plot4.pgl" to get an eps file

        implicit none
        integer I
        double precision cpar(10)
        integer, parameter :: N = 40000
        double precision z(N),dtc(N),eta(N),CBL(3,N),dts


        open(60,file='fig6')


! cosmological parameters
        call get_parameters(cpar)

! transverse comoving distance-redshift relation
        dts = -1d-4
        call torsion_trans_com_distance(cpar,dts,N,z,dtc,eta,CBL)


        do I=1,N
           if(mod(I,200).eq.1)  write(60,*) CBL(1,I),CBL(2,I),CBL(3,I)
        enddo


        end


! ==============================================================
        subroutine torsion_trans_com_distance(cpar,dts,N,z,dtc,eta,CBL)
        implicit none

        integer I,J,N
        double precision cpar(10)
        double precision step1,step2,Dc,Dci,Dcii,curv,dts
        double precision der1,der2,dp1,dp2,dp0,H,Hi,Hii,hp1,hp0

        double precision z(N),dtc(N),eta(N),CBL(3,N)

        integer, parameter :: Nx = 4
! X(1) is density, V(1) is time derivative of density
! X(2) is expansion, V(2) is the time derivative of expansion
! X(3) is the scale factor, V(3) is the time derivative of the scale factor
! X(4) is the comoving distance, and V(4) its time derivative
        double precision X(Nx),Xi(Nx),Xii(Nx),V(Nx),RK(Nx,4)

! tolerance for Omega_k to be considered zero
        double precision, parameter :: omk_tol = 1.0d-10

! initial density
        X(1) = cpar(8)
! initial expansion rate
        X(2) = 3.0d0*cpar(1)*(1+2.0*cpar(6))
! initial scale factor
        X(3) = 1.0d0
! initial comoving distance
        X(4) = 0.0d0
! auxillary
        Xi = X
        Xii= X
        CBL = 0.0d0
        RK = 0.0d0

        do I=1,N


! evolution:
        call get_V(cpar,Nx,X,V)
                  do J=1,Nx
                     RK(J,1) = dts*V(J)/X(2)
                     X(J) = Xi(J) + 0.5*RK(J,1)
                  enddo
        call get_V(cpar,Nx,X,V)
                  do J=1,Nx
                     RK(J,2) = dts*V(J)/X(2)
                     X(J) = Xi(J) + 0.5*RK(J,2)
                  enddo
        call get_V(cpar,Nx,X,V)
                  do J=1,Nx
                     RK(J,3) = dts*V(J)/X(2)
                     X(J) = Xi(J) + RK(J,3)
                  enddo
        call get_V(cpar,Nx,X,V)
          do J=1,Nx
             RK(J,4) = dts*V(J)/X(2)
             X(J)=Xi(J)+(RK(J,1)+2.0*(RK(J,2)+RK(J,3))+RK(J,4))/6.0d0
          enddo

! transverse comoving distance:
                if(cpar(4).gt.omk_tol) then
                   curv = (dsqrt(dabs(cpar(4))))*(cpar(1))
                   dtc(I)= 1d-3*((sinh(X(4)*curv))/curv)
                elseif(cpar(4).lt.-omk_tol) then
                   curv = (dsqrt(dabs(cpar(4))))*(cpar(1))
                   dtc(I)= 1d-3*((sin(X(4)*curv))/curv)
                elseif(abs(cpar(4)).le.omk_tol) then
                   dtc(I) = 1d-3*X(4)
                else
                   print *,'torsion_trans_com_distance: '
                   print *,'  bug in checking curvature.'
                   stop
                endif
                z(I) = (1.0/X(3)) - 1.0d0
                eta(I) = cpar(6)*log(1.0+z(I))

! calculating the Clarkson--Bassett--Lu (CBL) function in 2 different ways:
        step2 = ((1.0d0/Xi(3))-1.0) - ((1.0d0/Xii(3))-1.0)
        step1 = ((1.0d0/X(3))-1.0) - ((1.0d0/Xi(3))-1.0)
        H   =  X(2)/(3.0d0*(1.0d0 + 2.0d0*cpar(6)))
        Hi =   Xi(2)/(3.0d0*(1.0d0 + 2.0d0*cpar(6)))
        Hii =  Xii(2)/(3.0d0*(1.0d0 + 2.0d0*cpar(6)))
        hp1 = (H-Hii)/(step1+step2)
        hp0 = Hi
        CBL(1,I) = (1.0/Xi(3)) - 1.0d0

! directly from transverse Dc => CBL(2,:)
        Dc   = X(4)
        Dci  = Xi(4)
        Dcii = Xii(4)
        der2 = Dci - Dcii
        der1 = Dc - Dci
        dp1 = (der1+der2)/(step1+step2)
        dp2 = ((der1/step1) - (der2/step2))/step1
        dp0 = Dci
        CBL(2,I) = 1d0+hp0*hp0*(dp0*dp2 - dp1*dp1) + hp0*hp1*dp0*dp1

! from luminosity to transverse Dc = DL/(1+z) => CBL(3,:)
        Dc = X(4)*(1.0d0 + cpar(6)*log((1.0d0/X(3))))
        Dci = Xi(4)*(1.0d0 + cpar(6)*log((1.0d0/Xi(3))))
        Dcii = Xii(4)*(1.0d0 + cpar(6)*log((1.0d0/Xii(3))))
        der2 = Dci - Dcii
        der1 = Dc - Dci
        dp1 = (der1+der2)/(step1+step2)
        dp2 = ((der1/step1) - (der2/step2))/step1
        dp0 = Dc
        CBL(3,I) = 1d0+hp0*hp0*(dp0*dp2 - dp1*dp1) + hp0*hp1*dp0*dp1


        Xii = Xi
        Xi  = X
        enddo



        end

!=====================================================
        subroutine get_V(cpar,Nx,X,V)
        implicit none
        integer Nx
        double precision, intent(in)  :: X(Nx)
        double precision, intent(out) :: V(Nx)
        double precision lambda,torsion,hubble
        double precision cpar(10)

! X(1) = density
! X(2) = expansion
! X(3) = scale factor
! X(4) = comoving distance

        lambda  = cpar(7)
        hubble  = X(2)/(3.0d0*(1.0d0 + 2.0d0*cpar(6)))
        torsion = cpar(6)*hubble

        V(1)    = -1.0d0*X(1)*X(2)+ 4.0*torsion*( X(1) + lambda )
        V(2)    = -((X(2)*X(2))/3.0d0)-(X(1)/2.0d0)
        V(2)    =  V(2) + lambda + 2.0d0*X(2)*torsion
        V(3)    =  X(3)*hubble
        V(4)    =  -1.0d0/X(3)


        end subroutine

! ==============================================================
        subroutine FLRW_trans_com_distance(cpar,zf,dtc)
        implicit none

        integer I,n
        double precision cpar(10),curv
        double precision z,Om,Ok,Ol,Ho,dz,H,zz,zi,zf
        double precision d_rk1,d_rk2,d_rk3,d_rk4,di,d,dtc

! tolerance for Omega_k to be considered zero
        double precision, parameter :: omk_tol = 1.0d-10


         Ho = cpar(1)
         Om = cpar(2)
         Ol = cpar(3)
         Ok = cpar(4)

         n = 10
         dz = zf/(n*1d0)

         di = 0d0
         zi = 0d0
         d = di

         do I=1,n
            z = zi
            zz = 1d0 + z
            H = Ho*dsqrt(Om*(zz**3) + Ok*(zz**2) + Ol)
            d_rk1 = dz/H
            z = zi + 5d-1*dz
            zz = 1d0 + z
            H = Ho*dsqrt(Om*(zz**3) + Ok*(zz**2) + Ol)
            d_rk2 = dz/H
            d_rk3 = dz/H
            z = zi + dz
            zz = 1d0 + z
            H = Ho*dsqrt(Om*(zz**3) + Ok*(zz**2) + Ol)
            d_rk4 = dz/H

            d = di + (d_rk1+2d0*(d_rk2+d_rk3)+d_rk4)/6d0
            di = d
            zi = z
         enddo

         if(cpar(4).gt.omk_tol) then
            curv = (dsqrt(dabs(cpar(4))))*(cpar(1))
            dtc= 1d-3*((sinh(d*curv))/curv)
         elseif(cpar(4).lt.-omk_tol) then
            curv = (dsqrt(dabs(cpar(4))))*(cpar(1))
            dtc=  1d-3*((sin(d*curv))/curv)
         elseif(abs(cpar(4)).le.omk_tol) then
            dtc=  1d-3*d
         else
            print *,'FLRW_trans_com_distance: '
            print *,'  bug in checking curvature.'
            stop
         endif

         end

! ==============================================================
        subroutine get_parameters(cpar)
        implicit none
        double precision cpar(10)
        double precision eta_0, omega_phi,omega_k
        double precision omega_matter,omega_lambda,H0
        double precision pi, mu,lu,tu,gcons,cs,kap,kapc2,Ho,gkr,lb

        eta_0 = -0.03

        omega_matter = 0.3
        omega_lambda = 0.7
        omega_phi = -4.0*(1+eta_0)*eta_0
        omega_k = 1.0 - omega_matter - omega_lambda - omega_phi
        H0 = 70.0d0

!        print *, 'Omega_matter =', omega_matter
!        print *, 'Omega_Lambda =', omega_lambda
!        print *, 'Omega_k =', omega_k
!        print *, 'Omega_phi =',  omega_phi
!        print *, 'H_0 =', H0


! units: time in 10^6 years, length in kpc, mass in 10^15 M_{\odot}
        mu=1.989d45
        lu=3.085678d19
        tu=31557600*1d6
! and other constants
        pi = 4d0*datan(1.0d0)
        gcons= 6.6742d-11*((mu*(tu**2))/(lu**3))
        cs=299792458*(tu/lu)
        kap=8d0*pi*gcons*(1d0/(cs**4))
        kapc2=8d0*pi*gcons*(1d0/(cs**2))
        Ho=(tu/(lu))*H0
        gkr=3d0*(((Ho)**2)/(8d0*pi*gcons))
        lb=3d0*omega_lambda*(((Ho)**2)/(cs*cs))
        gkr=kapc2*gkr*omega_matter

        cpar(1) = Ho/cs
        cpar(2) = omega_matter
        cpar(3) = omega_lambda
        cpar(4) = omega_k
        cpar(5) = omega_phi
        cpar(6) = eta_0
        cpar(7) = lb
        cpar(8) = gkr
        cpar(9) = H0

        end
!=====================================================
